**docker-compose build**

At this moment, Docker will execute all configurations that we set up. When it’s done, you can launch your containers !

**docker-compose up -d**

How Docker works ?

Each container have one root access by default
So when you execute this :
**docker exec -it sf4_mysql_skeleton bash**

You are right now in the mysql container as root user. You can explorer this container as you want ;)
Symfony 4 here we are !

Now we know how use Docker, so let’s go to the PHP one and not as root, but as user : dev
**docker exec -it -u dev sf4_php_skeleton bash**

Now you are inside the php container with dev user and we have to get Symfony. Just to test, launch a php -v in this container. ;)
So let’s go to your home :

**cd /home/wwwroot/sf4**

Installation of Symfony4 with composer

**composer create-project symfony/skeleton my-temp-folder**

When it’s done, we will get the project to the root path.

**cp -Rf /home/wwwroot/sf4/my-temp-folder/. .**

**rm -Rf /home/wwwroot/sf4/my-temp-folder**


**Create mysql user for your app**

mysql> CREATE USER 'waheed'@'%' IDENTIFIED BY 'password';

Query OK, 0 rows affected (0.00 sec)

mysql> GRANT ALL PRIVILEGES ON * . * TO 'waheed'@'%';

Query OK, 0 rows affected (0.00 sec)

Mysql container password is “password” for waheed user.
